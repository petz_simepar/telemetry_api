/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.simepar.telemetry_api.jdbc;

import org.simepar.telemetry_api.dao.OffSetDAO;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.OffsetDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;
import org.simepar.telemetry_api.model.OffSet;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.*;
import org.springframework.stereotype.*;

/**
 *
 * @author Petz
 */
@Repository
public class OffSetJDBC implements OffSetDAO {

//  private static SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss.SSS");
//        
    DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm:ss");

    private static final Logger LOGGER = LoggerFactory.getLogger(OffSetJDBC.class);

    @Autowired
    private JdbcTemplate jdbcTemplate;

//    @Autowired
//    private TipoestacaoDAO tipoestacaoDAO;
    @Service
    class OffSetRowMapper implements RowMapper {

        @Override
        public Object mapRow(ResultSet rs, int i) throws SQLException {

            OffSet offset = new OffSet();
            offset.setCodigo(rs.getInt("estcodigo"));
            offset.setNome(rs.getString("estnome"));
            offset.setOffsetvalor(rs.getFloat("offvalor"));
            offset.setOrg_sigla(rs.getString("orgsigla"));
            offset.setOrgao(rs.getString("estorgao"));
            offset.setTipo(rs.getString("esttipo"));
            offset.setOffestacao(rs.getInt("offestacao"));
            offset.setOffsensor(rs.getInt("offsensor"));
            if(rs.getDate("estfimoperacao") != null){
                offset.setEstfimoperacao(rs.getDate("estfimoperacao"));
            } else {
                offset.setEstfimoperacao(null);
            }
            offset.setOffdatahora_str(formatter.format(rs.getObject("offdatahora", OffsetDateTime.class)));
            offset.setOffvalidode_str(formatter.format(rs.getObject("offvalidode", OffsetDateTime.class)));
            if (rs.getObject("offvalidoate") != null) {
                offset.setOffvalidoate_str(formatter.format(rs.getObject("offvalidoate", OffsetDateTime.class)));
            } else {
                offset.setOffvalidoate_str(null);
            }
            offset.setOffdatahora(rs.getObject("offdatahora", OffsetDateTime.class));
            offset.setOffvalidode(rs.getObject("offvalidode", OffsetDateTime.class));
            if (rs.getObject("offvalidoate") != null) {
                offset.setOffvalidoate(rs.getObject("offvalidoate", OffsetDateTime.class));
            } else {
                offset.setOffvalidoate(null);
            }

            return offset;
        }
        final OffSetJDBC this$0;

        OffSetRowMapper() {
            this$0 = OffSetJDBC.this;
        }
    }
    private final OffSetRowMapper offsetRowMapper = new OffSetRowMapper();

    @Override
    public List<OffSet> getOffSets(String estacoes) {
        String sql = "SELECT estcodigo, estnome, estcodigo, estorgao, estfimoperacao, orgsigla, esttipo, offvalor, offestacao, offdatahora, offvalidode, offvalidoate, offsensor "
                + "FROM telemetria.estacao, telemetria.orgao, telemetria.offsetregua "
                + "WHERE estorgao = orgcodigo "
                + "AND esttipo in(" + estacoes + ") "
                + "AND estcoleta = 'T' "
                + "AND estcodigo = offestacao "
                + "AND estorgao not in ( 11 , 12 ) "
                + "AND (offvalidoate IS NULL OR offvalidoate > now()) "
                + "ORDER BY orgsigla, estnome ";

        return (List<OffSet>) this.jdbcTemplate.query(
                sql,
                offsetRowMapper,
                new Object[]{}
        );

    }

    @Override
    public List<OffSet> getOffSetHistoric(int codigo) {
        String sql = "SELECT estcodigo, estnome, estcodigo, estorgao, estfimoperacao, orgsigla, esttipo, "
                    + "offvalor, offestacao, offdatahora, offvalidode, offvalidoate, offsensor "
                + "FROM telemetria.estacao, telemetria.orgao, telemetria.offsetregua  "
                + "WHERE esttipo in( 'H', 'F' )  ";
                if(codigo != 0){
                    sql += "AND offestacao=" + codigo + " "; 
                }
                    sql +=  "AND estcoleta = 'T' "
                    + "AND estcodigo = offestacao "
                    + "AND estorgao not in ( 11 , 12 ) "
                    + "AND estorgao = orgcodigo "
                    + "AND offvalidoate IS NOT NULL "
               + "ORDER BY offvalidode ASC";

        return (List<OffSet>) this.jdbcTemplate.query(
                sql,
                offsetRowMapper,
                new Object[]{}
        );
    }
        @Override
    public List<OffSet> getOffSetHistoric() {
        String sql = "SELECT estcodigo, estnome, estcodigo, estorgao, estfimoperacao, orgsigla, esttipo, "
                    + "offvalor, offestacao, offdatahora, offvalidode, offvalidoate, offsensor "
                + "FROM telemetria.estacao, telemetria.orgao, telemetria.offsetregua  "
                + "WHERE esttipo in( 'H', 'F' ) "
                    + "AND estcoleta = 'T' "
                    + "AND estcodigo = offestacao "
                    + "AND estorgao not in ( 11 , 12 ) "
                    + "AND estorgao = orgcodigo "
                    + "AND offvalidoate IS NOT NULL "
                + "ORDER BY offvalidode ASC";

        return (List<OffSet>) this.jdbcTemplate.query(
                sql,
                offsetRowMapper,
                new Object[]{}
        );

    }


    //IMPLEMENTAR INSERT DE OFFSETS
//    @Override
//    public OffSet setOffSets(OffSet offset) {
//        float tOffValor = offset.getOffsetvalor();
//        Timestamp tOffDataHora = Timestamp.from(Instant.now());
//        int tOffEstacao = offset.getOffestacao();
//        int OffSensor = offset.getOffsensor();
//        Timestamp tOffValidoate = Timestamp.from(Instant.now());
//        Timestamp tOffValidode = null;
//        
//        String sql = "INSERT INTO telemetria.offsetregua (offestacao,offdatahora,offvalor) "
//                    + "VALUES( "
//                + tOffEstacao + 
//                ", " + tOffDataHora +  
//                ", " + tOffValor +  
//                ")";
//        
//        
//        return null;
//    }
}
