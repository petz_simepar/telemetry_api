/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.simepar.telemetry_api.jdbc;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.format.DateTimeFormatter;
import java.util.List;
import org.simepar.telemetry_api.dao.OrgaoDAO;
import org.simepar.telemetry_api.model.Orgao;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;

/**
 *
 * @author santana
 */
@Repository
public class OrgaoJDBC implements OrgaoDAO{

    DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm:ss");

    private static final Logger LOGGER = LoggerFactory.getLogger(OffSetJDBC.class);

    @Autowired
    private JdbcTemplate jdbcTemplate;

//    @Autowired
//    private TipoestacaoDAO tipoestacaoDAO;
    @Service
    class OrgaoRowMapper implements RowMapper {

        @Override
        public Object mapRow(ResultSet rs, int i) throws SQLException {

            Orgao orgao = new Orgao();
            orgao.setOrgcodigo(rs.getInt("orgcodigo"));
            orgao.setOrgnome(rs.getString("orgnome"));
            orgao.setOrgsigla(rs.getString("orgsigla"));
            return orgao;
        }
        final OrgaoJDBC this$0;

        OrgaoRowMapper() {
            this$0 = OrgaoJDBC.this;
        }

    }
    private final OrgaoRowMapper orgaoRowMapper = new OrgaoRowMapper();

    
    @Override
    public List<Orgao> getOrgaos() {
       String sql = "SELECT orgsigla, orgnome, orgcodigo "
                + "FROM telemetria.orgao "
                + "ORDER BY orgcodigo ";

        return (List<Orgao>) this.jdbcTemplate.query(
                sql,
                orgaoRowMapper,
                new Object[]{}
        );
    }

}
