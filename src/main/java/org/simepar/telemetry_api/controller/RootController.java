/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.simepar.telemetry_api.controller;

import java.io.IOException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * @since 01/03/2018
 * @author Petz
 */
@RestController
public class RootController {

    @RequestMapping(value = "/home", method = RequestMethod.GET)
    public String home() {
        return "Home telemetry_api";
    }

    @RequestMapping(value = "/", method = RequestMethod.GET)
    public void swaggerUi(HttpServletRequest request, HttpServletResponse response) throws IOException {
        response.sendRedirect(request.getContextPath() + "/swagger-ui.html");
    }

}
