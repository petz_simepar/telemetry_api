/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.simepar.telemetry_api.controller;

import io.swagger.annotations.ApiOperation;
import java.util.List;
import org.simepar.telemetry_api.dao.OrgaoDAO;
import org.simepar.telemetry_api.model.Orgao;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author santana
 */
@RestController
@RequestMapping(value="/api")
public class OrgaoController {
    private final Logger LOGGER = LoggerFactory.getLogger(this.getClass().getName());

    @Autowired
    private OrgaoDAO orgaoDAO;
    
    @ApiOperation(value="Lista de Orgãos")
    @RequestMapping(value="/orgaos", method={RequestMethod.GET}, produces="application/json")
    public List<Orgao> getOrgaos(){
        return orgaoDAO.getOrgaos();
    }
    
    }
