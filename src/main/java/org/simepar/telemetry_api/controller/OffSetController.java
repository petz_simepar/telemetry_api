/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.simepar.telemetry_api.controller;

import io.swagger.annotations.ApiOperation;
import java.util.List;
import org.simepar.telemetry_api.dao.OffSetDAO;
import org.simepar.telemetry_api.model.OffSet;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
/**
 * @since 16/03/2018
 * @author Petz
 */

@RestController
@RequestMapping(value="/api")
public class OffSetController {
    private final Logger LOGGER = LoggerFactory.getLogger(this.getClass().getName());
    
    @Autowired
    private OffSetDAO offSetDAO;
    
    String estacoes = " 'H', 'F' ";
    
    
    @ApiOperation(value="Lista de OffSet's")
    @RequestMapping(value="/offset", method={RequestMethod.GET}, produces = "application/json")
    public List<OffSet> getOffSets(){
        return offSetDAO.getOffSets(estacoes);
    }
    
//    @ApiOperation(value="Histórico de OffSet's")
//    @RequestMapping(value="/offsethistoric", method={RequestMethod.GET}, produces = "application/json")
//    public List<OffSet> getOffSetHistoric(){
//        return offSetDAO.getOffSetHistoric();
//    }

    @ApiOperation(value="Histórico de OffSet's Por Codigo")
    @RequestMapping(value="/offsethistoric/{codigo}", method={RequestMethod.GET}, produces = "application/json")
    public List<OffSet> listOffSets(@PathVariable int codigo){     
        return offSetDAO.getOffSetHistoric(codigo);
    }


    // IMPLEMENTAR INSERT DE OFFSETS
    
//    @ApiOperation(value="Alteração de OffSet's")
//    @Requestmapping(value="/offset/insert", method={RequestMethod.POST})
//    public List<OffSet> setOffSets(){
//        return 
//    }
    
}

