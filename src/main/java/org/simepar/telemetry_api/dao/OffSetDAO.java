/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.simepar.telemetry_api.dao;

import java.util.List;
import org.simepar.telemetry_api.model.OffSet;

/**
 *
 * @author Petz
 */
public interface OffSetDAO {
    public List<OffSet> getOffSets(String estacoes);
    public List<OffSet> getOffSetHistoric(int codigo);
    public List<OffSet> getOffSetHistoric();
}
