/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.simepar.telemetry_api.dao;

import java.util.List;
import org.simepar.telemetry_api.model.Orgao;

/**
 *
 * @author santana
 */
public interface OrgaoDAO {
    public List<Orgao> getOrgaos();
}
    

