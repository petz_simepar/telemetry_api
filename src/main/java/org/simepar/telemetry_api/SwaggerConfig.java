/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.simepar.telemetry_api;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import static springfox.documentation.builders.PathSelectors.regex;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

/**
 *
 * @author daniel
 */
@Configuration
@EnableSwagger2
public class SwaggerConfig {

    @Bean
    public Docket api() {
        return new Docket(DocumentationType.SWAGGER_2)
                .select()
                //          .apis(RequestHandlerSelectors.any())              
                //          .paths(PathSelectors.any())                      
                .apis(RequestHandlerSelectors.basePackage("org.simepar.rest_telemetria.controller"))
                .paths(regex("/api.*"))
                .build()
                //.genericModelSubstitutes(Optional.class)
                .enableUrlTemplating(false);
    }
}
