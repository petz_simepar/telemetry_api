/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.simepar.telemetry_api.model;

import java.io.Serializable;
import org.apache.commons.lang.builder.ToStringBuilder;

/**
 *
 * @author santana
 */
public class Orgao implements Serializable{
    private int orgcodigo;
    private String orgsigla;
    private String orgnome;
    
    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

    public int getOrgcodigo() {
        return orgcodigo;
    }

    public void setOrgcodigo(int orcodigo) {
        this.orgcodigo = orcodigo;
    }

    public String getOrgsigla() {
        return orgsigla;
    }

    public void setOrgsigla(String orgsigla) {
        this.orgsigla = orgsigla;
    }

    public String getOrgnome() {
        return orgnome;
    }

    public void setOrgnome(String orgnome) {
        this.orgnome = orgnome;
    }

}
