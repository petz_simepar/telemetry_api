/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.simepar.telemetry_api.model;

import java.io.Serializable;
import java.sql.Date;
import java.time.OffsetDateTime;
import org.apache.commons.lang.builder.ToStringBuilder;

/**
 *
 * @author santana
 */
public class OffSet implements Serializable {

    private int codigo;
    private String nome;
    private String orgao;
    private String org_sigla;
    private String tipo;
    private Float offsetvalor;
    private int offestacao;
    private OffsetDateTime offdatahora;
    private OffsetDateTime offvalidode;
    private OffsetDateTime offvalidoate;
    private Date estfimoperacao;
    private String offdatahora_str;
    private String offvalidode_str;
    private String offvalidoate_str;
    private int offsensor;

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }
    
    //    DATAS {
    public Date getEstfimoperacao() {
        return estfimoperacao;
    }

    public void setEstfimoperacao(Date estfimoperacao) {
        this.estfimoperacao = estfimoperacao;
    }


    public String getOffdatahora_str() {
        return offdatahora_str;
    }

    public void setOffdatahora_str(String offdatahora_str) {
        this.offdatahora_str = offdatahora_str;
    }

    public String getOffvalidode_str() {
        return offvalidode_str;
    }

    public void setOffvalidode_str(String offvalidode_str) {
        this.offvalidode_str = offvalidode_str;
    }

    public String getOffvalidoate_str() {
        return offvalidoate_str;
    }

    public void setOffvalidoate_str(String offvalidoate_str) {
        this.offvalidoate_str = offvalidoate_str;
    }
    
    
    public OffsetDateTime getOffdatahora() {    
        return offdatahora;
    }

    public void setOffdatahora(OffsetDateTime offdatahora) {
        this.offdatahora = offdatahora;
    }

    public OffsetDateTime getOffvalidode() {
        return offvalidode;
    }

    public void setOffvalidode(OffsetDateTime offvalidode) {
        this.offvalidode = offvalidode;
    }

    public OffsetDateTime getOffvalidoate() {
        return offvalidoate;
    }

    public void setOffvalidoate(OffsetDateTime offvalidoate) {
        this.offvalidoate = offvalidoate;
    }

//    }
    public long getCodigo() {
        return codigo;
    }

    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getOrgao() {
        return orgao;
    }

    public void setOrgao(String orgao) {
        this.orgao = orgao;
    }

    public String getOrg_sigla() {
        return org_sigla;
    }

    public void setOrg_sigla(String org_sigla) {
        this.org_sigla = org_sigla;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public Float getOffsetvalor() {
        return offsetvalor;
    }

    public void setOffsetvalor(Float offsetvalor) {
        this.offsetvalor = offsetvalor;
    }

    public int getOffestacao() {
        return offestacao;
    }

    public void setOffestacao(int offestacao) {
        this.offestacao = offestacao;
    }

    public int getOffsensor() {
        return offsensor;
    }

    public void setOffsensor(int offsensor) {
        this.offsensor = offsensor;
    }

}
