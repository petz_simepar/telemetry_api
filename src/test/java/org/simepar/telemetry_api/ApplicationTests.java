package org.simepar.telemetry_api;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author santana
 */
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.simepar.telemetry_api.dao.OffSetDAO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = org.simepar.telemetry_api.Application.class)
public class ApplicationTests {

    private final Logger LOGGER = LoggerFactory.getLogger(this.getClass().getName());

    @Autowired
    private OffSetDAO offsetDAO;
    @Ignore
    @Test
    public void OffSet() {
        String estacoes = " 'H','F' ";

        System.out.println(offsetDAO.getOffSets(estacoes));

    }

}
